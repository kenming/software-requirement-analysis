### UML 應用於需求分析的技術

UML (Unified Modeling Language) 係由 OMG (Object Management Group) 所制定的一種以圖形作為軟體塑模 (modeling) 的標準。UML 2.X 規格總共制定了 14 種涵蓋軟體包括結構面與行為面的設計圖，其中適用於需求分析的設計圖主要有兩種：
* 活動圖 (activity diagram)：描述單一作業的業務流程。
* 使用案例模型 (use case model)：紀錄與描述系統功能。包含使用案例圖 (use case diagram) 與使用案例敘述 (use case description) 。

UML 活動圖比較適合描述單一作業流程內部的活動。所謂的「單一」業務流程係指在該流程內諸多活動的協力合作，以完成某一特定的企業目標 (specific business goal)。例如「請購流程」，它的目的在於：決定優先順位的供應商；而至於「採購流程」，它的目的在於：採購高品質的物料。

這些各有不同目的的作業流程，彼此之間可能會有某些關聯存在。例如上述的 「請購流程」，它會產出一請購確認的資訊，然後再將其傳入至「採購流程」。

利用活動圖並不適合描述有多個作業流程之間的關係，因為它揭露了作業流程內部的活動。描述過多的細節反而會失去多個作業流程之間關係表達的焦點。

適合描述多個作業流程的關聯的設計圖，可以利用UML for Erikson-Penker 企業擴充模型 (uml business extension model) 。下圖即為一個電子化採購系統關於「請購」與「採購」的「火箭圖」[註1]。

[註一] 因為 Erikson-Penker 語法內關於作業流程的圖示呈現的是一種 IPO (Input-Process-Output) 類似火箭的圖樣，所以簡稱為「火箭圖」。

圖例、請採購業務流程擴充模型圖 (多個作業流程的塑模)
<div align="center">
    <a href="/ch1/images/example-erikson-penker-diagram.png" target="_blank"><img src="/ch1/images/example-erikson-penker-diagram.png" alt="圖例、請採購業務流程擴充模型圖 (多個作業流程的塑模)" style="width:640px" /></a>
</div>

圖例、請購作業流程活動圖 (單一作業流程的塑模)
<div align="center">
    <a href="/ch1/images/example-activity-diagram.png" target="_blank"><img src="/ch1/images/example-activity-diagram.png" alt="圖例、請購作業流程活動圖 (單一作業流程的塑模)" style="width:640px" /></a>
</div>

至於資訊系統的系統功能分析，則以使用案例圖 (use case diagram)來作為塑模的表達。它很容易可以界定出系統開發/維護範圍、外界 (使用者/外部系統)與系統的關係。

圖例、電子化採購系統使用案例圖 (系統功能的塑模)
<div align="center">
    <a href="/ch1/images/example-use-case-diagram.png" target="_blank"><img src="/ch1/images/example-use-case-diagram.png" alt="圖例、電子化採購系統使用案例圖 (系統功能的塑模)" style="width:640px" /></a>
</div>

另外也會使用系統循序圖 (system sequence diagram)，表達參與者 (actor，一般指使用系統的人/角色，以及支援服務的外部系統。) 與系統之間的互動訊息 (message)。

系統循序圖是一種系統功能「實現 (realization)」的表達。如下圖 描述的是「Web ATM」系統其中「轉帳」系統功能的實現。系統分析師可以用此來表達參與者與系統/外部系統之間的訊息傳遞的互動關係。

圖例、實現「轉帳」系統功能的系統循序圖 (描述系統功能實現的塑模)
<div align="center">
    <a href="/ch1/images/example-system-sequence-diagram.png" target="_blank"><img src="/ch1/images/example-system-sequence-diagram.png" alt="圖例、實現「轉帳」系統功能的系統循序圖 (描述系統功能實現的塑模)" style="width:640px" /></a>
</div>