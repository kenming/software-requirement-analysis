### 物件合作

實現 (realize) 軟體資訊系統功能，從物件導向 (object-oriented) 的觀點來看待時，系統內部的主要組成元素是「物件」，也可以稱為「個體 (instance)」。可以想像軟體系統內部就是由擔任各種不同職掌的類型物件，為實現某一特定功能，在動態期間 (run-time) 的互動合作來協力完成。

關於物件如何分類 (classify)，這就是所謂物件導向結構設計的議題。分類作得好，讓系統具有低耦合 (low coupling) 、高內聚 (high-cohesion)的特性，如此系統的應變更具彈性、延展性，並得以提昇再利用性的高度價值。

參考下圖範例，是利用 UML 循序圖 (sequence diagram) 表達實現「餐飲管理系統」其中「點餐」系統功能的物件合作 (object collaboration) 情形。程式開發人員應該可以很容易對應至程式碼的類別 (class) 與方法 (method) 的實作。

<div align="center">
    <a href="/ch1/images/example-dining-sequence-diagram.png" target="_blank"><img src="/ch1/images/example-dining-sequence-diagram.png" alt="圖例、實現「點餐」系統功能的物件合作循序圖" style="width:640px" /></a>
</div>

軟體結構設計是屬於系統的內部結構設計議題，而需求分析則是站在系統外部的觀點來看待系統所提供的服務 (系統功能)，這是完全不同的兩種構面，不能混為一談。一般在需求分析階段，是會把系統當成「黑箱 (black blox)」來看待，只專注在外界 (人或外部系統) 如何與系統的溝通互動，至於系統內部如何組織分類與實作，那就會由結構與程式設計師來負責的。