### 系統功能

系統 (system) 是由一組互動 (interacting) 或獨立的元件 (component) 所組成的整合體 (integrated whole)。

系統功能 (system functions)，即為某一整合體所提供給外界 (參與者或外部系統)的一連串服務 (services)。

把企業 (business) 當成一個整合體，並藉以分析該整合體所提供的功能需求，以及組成該整合體的組成元素。這樣以企業為標的的分析塑模方式，稱為「企業塑模 (business modeling)」。

把資訊系統 (information system) 當成一個整合體，並藉以分析該整合體所提供的系統功能，以及組合該整合體的結構元素。這樣以資訊系統為標的的分析塑模方式，稱為「資訊系統塑模 (information system modeling)」。

把企業或資訊系統當為一個整合體，便可以採用相同的手法，如利用 UML (Unified Modeling Language) 語法，來分析系統需求。

「企業」與「資訊系統」兩者的關係即為，「資訊系統」為組成「企業」內部結構的其中之一的元素 (element)。而當把焦點轉移至資訊系統，探究其中所提供的系統需求 (系統功能)時，則再將「資訊系統」這個結構元素放大，並從「外」的需求分析，與「內」的結構設計來看待資訊系統的分析、設計，乃至於實作了。

參考上節範例，「診療服務」為企業的系統需求，如果企業架構師規劃了三個資訊系統 (組成元素) 的支援，參考如下圖，架構師可以利用 UML 使用案例圖表 (use case diagram) 規劃每一個資訊系統各有其需實現的系統功能，以及資訊系統之間的關係是透過介面 (interface)的連接達成所謂的「SOA (service-oriented architecture)」架構。

<div align="center">
    <a href="/ch1/images/example-hospital-system-use-case.png" target="_blank"><img src="/ch1/images/example-hospital-system-use-case.png" alt="圖例、表達資訊系統的系統功能" style="width:640px" /></a>
</div>

至於資訊系統是如何實現系統功能，又如何連接其它外部系統，那就是所謂的系統 （這裡就是指資訊系統了) 結構設計與實作的議題了。