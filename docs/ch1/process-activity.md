### 流程活動

實現企業層級系統功能的主要組成元素是「人」，也就是多個不同角色的內部工作者 (internal worker) 在不同的時間點合作接續以完成所屬職掌的工作。

當為了履行一個企業需求，可能需一連串的活動 (activities) 循序接力達成。當然活動的執行需要有特定的參與人員，也就是會有多種不同角色 (role) 或組織 (organization) 參與。這一連串的活動即構成作業流程 (business process)。

Jacobson 為企業流程 (business process) 作了一個簡單的定義：

Put simply, a business process is the set of internal activities performed to serve a customer.  
*《Object Advantage》, Ivar Jacobson, 1994*  
（簡單地說，企業流程就是要能夠服務客戶的一連串企業內部的活動）

而關於「活動 (activity)」的定義 (from Wiki)：

Activity is a major task that must take place in order to fulfill an operation contract.  
(活動是一個必須履行的主要工作，為以滿足一個操作上的契約。)

參考下圖範例係利用 UML 活動圖 (activity diagram) 繪製傳統人工「訂購－出貨」作業流程。從該案例可以得知，這一連串從訂購到出貨的活動，會有多個不同組織部門的工作人員參與。當完成這一連串活動後，即滿足主要的企業需求－「訂購商品」。

<div align="center">
    <a href="/ch1/images/example-order-shippemnt-activity-diagram.png" target="_blank"><img src="/ch1/images/example-order-shippemnt-activity-diagram.png" alt="圖例、傳統人工訂購－出貨作業流程" style="width:640px" /></a>
</div>


