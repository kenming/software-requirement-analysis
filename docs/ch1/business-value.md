### 企業價值

企業需求要能提供價值 (value)，如此才得以讓企業有獲利行為而能永續經營。

「價值」的呈現取決於在創新、成本、效率等因子的調和。這些因子經常是由內部工作者 (internal-worker)、產品、系統、軟體與流程 (process)等所提供並滿足其需求。最終目的當然在於讓企業創造「利潤 (profit)」。

例如，「仁心慈善醫院」是一家企業 (business)，其中最主要的核心價值係提供 「醫療」的服務 (service)。為了提供「醫療」這個主要服務，企業內部必然會有多個內部工作者的協同合作，包括「醫師」、「護士」、「藥劑師」、「行政人員」… 等；涵蓋的作業流程可能有「掛號」、「看診」、「住出院」、「領藥」… 等；且為了增進處理效率與節省人力，會導入由 產品/軟硬體 所組成的資訊系統 (information system)，成為工作者的協力夥伴。

除了文字陳述紀錄外，可以藉由視覺化的圖形塑模 (visualization diagram modeling)，更容易表達焦點。

例如，我們可以使用 UML「使用案例圖 (use case diagram)」表達企業所提供的主要服務 (企業層級的系統功能)，以及使用「業務流程圖 (business process diagram)」表達實現 （realize) 企業系統功能的主要組成元素 (內部工作者、資訊系統、主要作業流程)。

<div align="center">
<a href="/ch1/images/example-hospital-business-use-case.png" target="_blank"><img src="/ch1/images/example-hospital-business-use-case.png" alt="圖例、表達企業提供的主要服務" style="width:640px" /></a>

<a href="/ch1/images/example-hospital-business-process.png" target="_blank"><img src="/ch1/images/example-hospital-business-process.png" alt="圖例、表達實現企業需求內部的主要組成元素" style="width:640px" /></a>
</div>

關於 UML (unified modeling language) 的基本介紹，以及應用在企業與資訊系統需求面的分析 Know-how，參閱後述的章節內容。