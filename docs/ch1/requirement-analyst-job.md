### 需求分析人員的職掌

針對「需求」作描述、紀錄與分析的人員統稱為「需求分析師 (requirement analyst, RA)」。

而以「企業」或「資訊系統」作為分析的標的來區分，需求分析人員的角色 (role) 可分為兩種：企業分析師 (business analyst, BA) 與資訊系統分析師 (information system analyst, 簡稱 SA)。

BA 涵蓋的分析範疇較 SA 來得廣，自然需較有整體性的架構觀；但相對細節，如對某一資訊系統的畫面操作或單一業務規則 (business rule)，則無法來得比 SA 精細。

一般而言，BA 涵蓋的範疇有：
* 企業策略的規劃。
* 業務流程的制定與再造 (BPR, Business Process Re-engineering)。
* 企業的塑模 (business modeling)。
* 多個資訊系統之間的訊息交換與整合。

而對於資訊系統分析師 SA，一般較僅以單一資訊系統為分析的範疇：
* 紀錄與描述未來某些活動 (activities) 會由資訊系統實現的作業流程。
* 界定系統功能與非系統功能需求 (functional and non-functional requirements)。
* 表單畫面的操作程序。
* 表單需要的欄位資訊與業務規則的整理。

對於資訊系統的技術議題，SA 係從系統外觀 (把系統當黑箱) 看待系統所提供的服務 (系統功能)，以及實現該功能的操作程序、業務邏輯與所需欄位資訊等。所以 SA 一般不會涉及到結構性的設計議題，諸如資料庫的資料模型 (data model) 設計，那是由 SD (System Designer) 所負責；以及程式寫碼的實作議題，那是由 Programmer 負責。

再則 BA/SA 一般並不需具備某一領域 (domain) 的相關知識，那應是由領域專家 (domain expert) 所負責。但BA/SA 需能具備與領域專家溝通的技能，例如了解該領域所常使用的詞彙術語與其意涵。

另 BA/SA 合作的對象除了領域專家外，當然會包括各類開發人員，諸如專案經理 (Project Manager)、SD (System Designer)、Programmer (程式設計師) …等。

而訪談的對象也包括了客戶單位的利益關係人 (stakeholder)、操作人員 (operator) …等。

簡而言之，BA/SA 最大的挑戰在於如何與各類不同角色的人員達成順暢的溝通，並進而引導發掘出其潛在甚或模糊的需求，且得以條理分明地讓開發人員理解而轉成實作的程式。