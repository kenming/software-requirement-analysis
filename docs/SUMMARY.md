# Summary

* [簡介](README.md)
* 第一章、需求分析概觀
    * 1.1. 企業與資訊系統的關係
        * [1.1.1. 企業價值](ch1/business-value.md)
        * [1.1.2. 系統功能](ch1/system-function.md)
        * [1.1.3. 流程活動](ch1/process-activity.md)
        * [1.1.4. 物件合作](ch1/object-collaboration.md)
    * [1.2. 需求分析人員的職掌](ch1/requirement-analyst-job.md)
    * [1.3. 區分功能與非功能性需求 ](ch1/functional-vs-nonfunctional-requirement.md)
    * [1.4. UML 應用於需求分析的技術](ch1/uml-for-requirement-analysis.md)
* 第二章、企業流程分析與塑模
    * [2.1. 企業層級系統塑模的範疇](ch2/business-modeling-scope.md)
    * [2.2. 跨多個作業流程的塑模](ch2/across-multiple-process-modeling.md)
    * [2.3. 單一作業流程的塑模](ch2/single-process-modeling.md)
* [第三章、系統功能分析與塑模](ch3/README.md)
* [第四章、需求陳述紀錄與整理](ch4/README.md)
* [第五章、快速實現系統功能](ch5/README.md)
* [附錄](appendix/appendix.md)