### 跨多個作業流程的塑模

每一個作業流程，有各自不同特定的企業目的 (specific business goal)。例如：
* 訂貨流程的特的目的為讓交易有效率且安全可靠。
* 出貨流程的特的目的為及早可將貨品交付到客戶手中。
* 採購流程的特的目的為從供應商取得低成本、高品質的商品。

企業經營者/高階管理者、系統相關的利益關係人 (stakeholder)、乃至於系統開發團隊，希能透過這些作業流程看到較巨觀 (macro view)、更具整體的全貌 (Whole View)，如此可得以有效地作整體性的架構規劃與設計。

跨多個作業流程，就是流程範圍更為廣泛，參與的人更多，所涵蓋的時間更長。而且往往因為現實上基於功能執掌，還是不同部門或不同地點就有不同的作業規範與範疇，但彼此這些作業之間仍需要連串在一起。

由Erickson以及Penker所提出的一個活動圖的擴充型態（stereotype），稱之為「Erikson-Penker 企業擴充模型」（Erikson-Penker Business Extension）。（《Business Modeling with UML: Business Patterns at Work》, Manus Penker & Hans-Erik Erikson, 2000, p.57），該設計圖形最適切用來描述跨多個作業流程之間的關聯與各自的企業目的。

這個圖形中，主要是將與作業流程相關的重要人、事、物以及這個流程所要達成的目標做一個連結；不過有關企業流程的內部細節，通常在這張圖中不會有過多的介紹，以免失焦 (內部的活動細節會由 uml 活動圖表達)。


#### Erikson-Penker企業擴充模型簡介

下圖即對 Erikson-Penker (底下簡稱火箭圖) 的企業擴充模型的重要元素作說明。

<div align="center">
    <a href="/ch2/images/erikson-penker-notation.png" target="_blank"><img src="/ch2/images/erikson-penker-notation.png" alt="圖、Erikson-Penker 企業擴充模型的主要圖形元素" style="width:640px" /></a>
</div>

&nbsp;
&nbsp;

*   處理（Process）  

    在這張擴充模型中，「處理」代表一連串有意義的工作流程（Workflow），在一個處理中，通常要達成一個或多個的「目標」，以指出該「處理」在企業上的意義為何。  

    「處理」是該擴充模型的核心，在處理中，會有「輸入」、「輸出」以及「參與」。通常一個「處理」會搭配一張明細的「活動圖」以描述該工作流程的細節。  
    
    <div style="display:inline;">「處理」的圖示是一個「</div>
    <div style="display:inline;"><img src="/ch2/images/erikson-penker-notation-process-icon.png" alt="圖、Erikson-Penker 處理圖示" style="width:96px" /></div>
    <div style="display:inline;">」的圖形。</div>
&nbsp;

*   目標 （Goal）  

    「目標」是 Erikson-Penker 企業擴充模型與一般傳統的「輸入-處理-產出」（Input-Process-Output; IPO） 模型最大的差異處。在每一個「處理」中，最好能夠明確表達出該「處理」所「達成」（achieve） 的「企業價值」為何？而這個「企業價值」，可以用一個「目標」來代表。  

    <div style="display:inline;">「目標」的圖示是標準UML物件圖示的一個「擴充型別」（stereotype）：「</div>
    <div style="display:inline;"><img src="/ch2/images/erikson-penker-notation-goal-icon.png" alt="圖、Erikson-Penker 目標圖示" style="width:64px" /></div> 
    <div style="display:inline;">」。</div>
&nbsp;

*   達成 （Achieve）關係  

    「達成」關係主要是連接「處理」與「目標」間的關係。  

    <div style="display:inline;">「達成」關係的圖示也是「相依」圖示的擴充型別：「</div>
    <div style="display:inline;"><img src="/ch2/images/erikson-penker-notation-achieve-icon.png" alt="圖、Erikson-Penker 達成圖示" style="width:64px" /></div> 
    <div style="display:inline;">」。</div> 
&nbsp;    

*   事件 （Event）  

    「事件」指的是對企業而言，在特殊的時間點，會引發一連串企業內部反應的外部行為（happening）。通常來說，對於「處理」而言，「事件」是最常被當作是「處理」的重要「輸入」（input）的來源。  

    <div style="display:inline;">事件的圖示是「</div>
    <div style="display:inline;"><img src="/ch2/images/erikson-penker-notation-event-icon.png" alt="圖、Erikson-Penker 事件圖示" style="width:64px" /></div> 
    <div style="display:inline;">」。</div>    
&nbsp;

*   資訊（Information）  

    「資訊」指的是對企業而言，有意義的一群資料的集合。「資訊」可以是「處理」的「輸入 （input）」、處理的中介「參與 （supply）」，甚或是「處理」的「產出 （output）」。  

    <div style="display:inline;">「資訊」的圖示為一個平行四邊形「</div>
    <div style="display:inline;"><img src="/ch2/images/erikson-penker-notation-information-icon.png" alt="圖、Erikson-Penker 資訊圖示" style="width:64px" /></div> 
    <div style="display:inline;">」。</div>   
&nbsp;

*   物件 （Object）     

    「物件」指的是在企業中確實存在的實體物品，有可能是一張特定的實體表單，也有可能是一個特定的產品。相同地，「物件」也可以是處理的輸入、參與或是產出。   

    <div style="display:inline;">「物件」的圖示是標準的UML表示法：「</div>
    <div style="display:inline;"><img src="/ch2/images/erikson-penker-notation-object-icon.png" alt="圖、Erikson-Penker 物件圖示" style="width:64px" /></div> 
    <div style="display:inline;">」。</div>    
&nbsp;

*   參與者 （Worker）  

    「參與者」指的是企業中的特定的「角色」或「組織部門」。對於「處理」來說，「參與者」只會是中介參與。  

    <div style="display:inline;">「參與者」的圖示為「</div>
    <div style="display:inline;"><img src="/ch2/images/erikson-penker-notation-worker-icon.png" alt="圖、Erikson-Penker 參與者圖示" style="width:64px" /></div> 
    <div style="display:inline;">」。</div>    
&nbsp;

*   相依（Dependency）關係  

    「事件」、「參與者」、「輸入物件」以及「輸入資訊」與「處理」之間的關係都是屬於「相依」關係，這代表著「處理」與這些重要元素間具備「參考」關係。  
    <div style="display:inline;">「相依」的圖示是標準的UML圖示：「</div>
    <div style="display:inline;"><img src="/ch2/images/erikson-penker-notation-dependency-icon.png" alt="圖、Erikson-Penker 相依圖示" style="width:64px" /></div> 
    <div style="display:inline;">」。</div>    
&nbsp;

*   供應 （Supply）關係  

    「處理」過程中，若有需要的「資訊」或是「物件」時，則兩者之間的關係是屬於「供應」關係。  

    <div style="display:inline;">「供應」關係的圖示是UML「相依」圖示的一個擴充型別：「</div>
    <div style="display:inline;"><img src="/ch2/images/erikson-penker-notation-supply-icon.png" alt="圖、Erikson-Penker 供應圖示" style="width:64px" /></div> 
    <div style="display:inline;">」。</div>    
&nbsp;


#### 案例－進銷存系統的進貨/退貨作業流程

下圖為某商場的進銷存資訊系統關於「進貨－退貨」作業流程圖。可以看出該火箭圖表達了兩個主要的作業流程：   
*   「進貨流程」：係由供應商提供進貨商品的事件而啟動該作業流程。
*   「退貨流程」：係經由客服部門提供退貨商品的事件而啟動該作業流程。

<div align="center">
    <a href="/ch2/images/example-purchase-return-business-process.png" target="_blank"><img src="/ch2/images/example-purchase-return-business-process.png" alt="圖例、進銷存系統進/退貨作業流程" style="width:640px" /></a>
</div>

進貨/退貨會區分為兩個作業流程的主要原因即為：各有著不同的「達成目標」。
*   進貨流程的主要達成目標在於：增進進貨效率與品質。
*   出貨流程的主要達成目標在於：保障客戶權益與提昇商品退貨處理效率。

諸多軟體分析人員，常常忽略了這種「目標導向」的分析方式，因而在流程分割時，往往缺乏了一個依循的標的，而往往落入細節而忘卻了整體規劃的核心。

除此之外，對於跨多個作業流程的表達 (使用火箭圖)，其最大的特色就在於其描述了企業中重要的人、事、物與流程的關係。在這個階段，並不會牽涉到流程的細節。

事實上，若是到分析流程細節時，因為重點反而要擺在「流程」本身，因此除了參與流程的角色外，其他包括事件以及實體的物件 （包括表單、資料等） 都不會呈現在流程圖中，這也可以讓流程的分析設計人員可以分不同的層次來進行分析。