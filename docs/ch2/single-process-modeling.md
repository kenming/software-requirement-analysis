### 單一作業流程的塑模

描述與紀錄單一作業流程內部的一連串活動，使用 UML 活動圖 (activity diagram) 是最為適切的。

依據 UML 三巨頭的論述，活動圖主要的目的在陳述活動與活動之間的流程控制的轉移 (control flow transition)。 
Activity diagrams emphasize the flow of control from activity to activity. （《The Unified Modeling Language User Guide》, Grady Booch, James Rumbaugh & Ivar Jacobson, 1999, pp. 257）

這裡所謂的活動，可以指企業的活動，也可以指應用程式中的某個特定功能。 

不過一般來說，由於「活動」的定義並不如「物件」那麼明確，因此，在進行系統設計時，一般比較不傾向於利用活動圖來表達應用程式的結構 (採用物件模型較為恰當)；也因此，活動圖通常會比較適合用於表達企業活動的工作流程關係。除此之外，由於活動圖非常類似傳統的流程圖 (flow-chart)，因此，活動圖也適於表達細部程式的程序性結構。 

參考下圖，藉由一個請假作業流程來說明 UML 活動圖的主要圖形元素。

<div align="center">
    <a href="/ch2/images/activity-notation-by-askleave-process-example.png" target="_blank"><img src="/ch2/images/activity-notation-by-askleave-process-example.png" alt="圖、請假作業流程活動圖的基本圖示語法" style="width:640px" /></a>
</div>

&nbsp;
&nbsp;

*   起始點 （Activity Initial）

    起始點指的是一連串活動的開始點。在一張活動圖中，必須要且只能夠有一個起始點。
    <div style="display:inline;">起始點的圖示是「</div>
    <div style="display:inline;"><img src="/ch2/images/activity-initial-icon.png" alt="圖、活動圖-起始點圖示" style="width:36px" /></div> 
    <div style="display:inline;">」。</div>    
    &nbsp;

*   結束點 （Activity Final）
    結束點指的是一連串活動的終結點。在一張活動圖中，可以有多個的結束點。
    <div style="display:inline;">結束點的圖示是「</div>
    <div style="display:inline;"><img src="/ch2/images/activity-final-icon.png" alt="圖、活動圖-結束點圖示" style="width:36px" /></div> 
    <div style="display:inline;">」。</div>    
    &nbsp;

*   活動 （Activity）
    
    活動是活動圖中最核心、最重要的一個元素。前述已提及關於「活動」的定義：一個必須履行的主要工作，為以滿足一個操作上的契約。
    
    活動的命名必然是「動+名詞」的詞性，以突顯它表達的是要 ”做” 的 “事情” (do something)。
    
    界定單一的活動是依據在某一特定期間 (specific time) 內所作的事情。

    例如：「醫院櫃台人員處理病患的掛號服務」就是一個活動。在這一特定期間內，櫃台人員在該活動的主要目的為：「處理掛號」，因此就以該目的為該活動來命名。

    許多需求分析人員對於「活動」的界定沒有一個很明確的定義，因此而導致有時活動很大，例如把子系統/模組當成活動；或者把某一個只是細部的操作細節當成活動，例如計算訂單總額。這些都相當不適切。

    另外活動必然是「服務導向 (service oriented)」，標榜的是「要做的事情」。所以也不能把資料本身當成活動主體，這也是需求人員常會出現的問題。把資料當成主體的分析方式是屬於「DFD (Data Flow Definition)」的方法論，但因過早揭露資料的細節，導致開發的複雜性提昇：而「服務導向」的方式則是把資料「封裝 (encapsulate)」於活動的內部，當確認開發的主軸後，再逐一揭露處理的細節。由簡入繁，避免陷入細節紛爭，如此可大幅降低系統開發的風險。

    <div style="display:inline;">活動圖中的 Activity 的表達圖示是「</div>
    <div style="display:inline;"><img src="/ch2/images/activity-icon.png" alt="圖、活動圖圖示" style="width:72px" /></div> 
    <div style="display:inline;">」。</div>    
    &nbsp;

*   動作 （Action）
    「動作」必然是包含於「活動」的主體之內，以突顯每一個「動作」是「單一 (atomic)」的工作項目 (work item)，一個「活動」會有一到多個「動作」。

    例如，前述櫃台人員在活動－「處理掛號」的期間內，會處理多個工作事項 (也就是多個動作)，包括了查詢病患資訊、收取掛號費用與健保卡、列印單據等。

    <div style="display:inline;">活動圖中的 action 的表達圖示是「</div>
    <div style="display:inline;"><img src="/ch2/images/activity-action-icon.png" alt="圖、活動圖-action 圖示" style="width:72px" /></div> 
    <div style="display:inline;">」。</div>    
    &nbsp;

*   轉移 （Transition）

    轉移代表著流程控制權 （Flow Control） 的移轉。當某一個活動結束後，流程的控制權透過 Transition 表達出其移轉給另外一個活動。
 
    <a href="/ch2/images/transititon-notation.png" target="_blank"><img src="/ch2/images/transititon-notation.png" alt="圖、轉移的表示法" style="width:320px" /></a>

*   決策 （Decision）
    「決策」則代表一個判斷的準則，其表現方式，則是以「 」來表示。
 
    <a href="/ch2/images/decision-notation.png" target="_blank"><img src="/ch2/images/decision-notation.png" alt="圖、決策的表示法" style="width:360px" /></a>
    
    上圖當指定了一個分支時，此時，從分支連接出去的轉移必須要有條件判斷式，這在UML中稱之為「限制」 （Constraint），在UML中的表示法中，限制的表達方式是用一個中括弧「[]」來表示該轉移必須要有條件的限制。

*   分派 （Fork） 以及會合 （Join）

    分派以及會合主要代表對於後續活動的同步處理。當某個活動結束後，需要同時進行兩個以上的活動（如工作流程中的會簽），則此時，必須利用「分派」來加以表達；而當某個活動必須要等待其前置的多個活動結束後方可進行，此時，則利用「會合」來表達。「分派」以及「會合」的表達圖示，都是「 」的圖形。

    <a href="/ch2/images/fork-join-notation.png" target="_blank"><img src="/ch2/images/fork-join-notation.png" alt="圖、分派以及會合的表示法" style="width:320px" /></a>

    一般來說，在UML活動圖中，分派與會合通常會是搭配一起出現，也就是說，若是你在繪製活動圖時，加入了一個分派點，那麼，活動到了某個特定的地方，多半會有一個會合點，這對於繪製活動圖的檢核來說，是一個非常容易判別的參考點。

*   分割區塊（Partition）

    分割區塊 （Partition） 在整個活動表達上，是一個非常重要的概念。我們可以利用分割區塊來將活動分配給對應的角色。以上述請假案例圖為例，由於表達了分割區塊，因此可以清楚地知道「處理請假事宜」的這個活動，主要是由「人事部門」這個角色的人來發動並執行
    <div style="display:inline;">活動圖中的 partition 的表達圖示是「</div>
    <div style="display:inline;"><img src="/ch2/images/activity-partition-icon.png" alt="圖、活動圖-partition 圖示" style="width:36px" /></div> 
    <div style="display:inline;">」。</div>    
    &nbsp;    


#### 案例1－進銷存系統進貨/退貨作業流程的活動圖

參考下圖一為進貨作業流程的活動圖；另一為退貨作業流程的活動圖。

<div align="center">
    <a href="/ch2/images/case1-1-purchase-order-activity-diagram.png" target="_blank"><img src="/ch2/images/case1-1-purchase-order-activity-diagram.png" alt="圖、進貨作業流程的活動圖" style="width:640px" /></a>
</div>

<div align="center">
    <a href="/ch2/images/case1-2-return-order-activity-diagram.png" target="_blank"><img src="/ch2/images/case1-2-return-order-activity-diagram.png" alt="圖、退貨作業流程的活動圖" style="width:640px" /></a>
</div>

在繪製活動圖時，必須要特別注意一個重點，亦即：活動圖的主要核心是「活動」，因此在活動圖中，要盡可能地把「中介產出文件」（包括表單、報表等）排除在外。

繪製活動圖比較好的方式是要和領域專家直接面對面溝通，並且最好在訪談過程中直接繪製活動圖。根據活動圖覆述一次你在訪談中所蒐集到的相關資訊，如此一來，活動圖所蒐集到的資訊比較不會失真。

但要切記，在繪製活動圖時，千萬不要落入到活動的細節，活動圖所需要捕捉的，是整體業務流程的「大方向」，而非某個單一活動的準確度或是其相關的企業規則 （Business Rule），有關細節的相關描述，應該是在討論「使用案例」時才需要捕捉，若是過早介入流程細節，需求收集 （Requirement Acquisition） 很容易就陷入到分析癱瘓的陷阱，在繪製活動圖時不可不慎。


#### 案例2－工作流程文字描述轉視覺化的流程活動圖

下述是一個真實的案例。客戶只給幾行的文字敘述，就希望開發單位實作出適宜該單位運作的工作簽核系統。

> 客服人員接到信用卡客戶要求臨時調高信用額度，客服人員詢問調整原因並向客戶詢問身份字號、出生日、聯絡住址等登錄之基本資料以確認是否本人，並調閱繳款記錄是否異常。然後詢問客戶打算調高之額度為何及調整之期間多久。確認完畢後，即送出案件，啟動客服流程。

> 案件送出，即通知客服後勤人員處理，客服後勤人員從工作駐列(Queue)中取得案件後，判斷臨時調高之信用額度是否為客服人員被授權的額度內，若是，則以電話聯絡客戶再確認是否有提出申請，確認完畢即結束流程。

> 若臨時調高之額度高於客服人員之授權，則後勤人員以電話聯絡客戶後，向客戶確認是否申請並解釋他被授權所能調整之額度，若客戶仍需超過額度，則後勤人員將案件轉呈客服主管處理，客服主管收到案件後，回電客戶確認資料並決定授權金額，確認完畢後即結束流程。

開發單位若僅依此文字描述就據此開發，很容易就產生解讀上的誤解。而若需求分析人員可以先轉化至圖形化的表達，如此可以先行與客戶單位確認需求的正確性，相關開發人員也比較容易理解而不致耗費太多時間在不必要的猜測解讀，甚而作白工。

參考下圖，上述的文字敘述其實就是在描述一段事務流程，主要的目的在於提供顧問「臨時調高信用額度」的企業服務。
這是轉化為 UML 活動圖後的視覺化呈現，是利用文字敘述，抑或是採以設計圖表達，哪種容易讓開發人員解讀，一目了然。這也是所謂「塑模 (modeling)」的威力之所在。

<div align="center">
    <a href="/ch2/images/case2-temporay-adjust-credit-activity-diagram.png" target="_blank"><img src="/ch2/images/case2-temporay-adjust-credit-activity-diagram.png" alt="圖、臨時調高信用額度的作業流程活動圖" style="width:640px" /></a>
</div>
